package ru.t1.amsmirnov.taskmanager.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.amsmirnov.taskmanager.command.AbstractCommand;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public final class FileScanner extends Thread{

    @NotNull
    private final Bootstrap bootstrap;

    @NotNull
    private final List<String> commands = new ArrayList<>();

    @NotNull
    private final File fileFolder = new File("./");

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        setDaemon(true);
    }

    @Override
    @SneakyThrows
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            Thread.sleep(3000);
            process();
        }
    }

    @Override
    public synchronized void start() {
        @NotNull final Iterable<AbstractCommand> commands = bootstrap.getCommandService().getCommandWithArgument();
        commands.forEach(e -> this.commands.add(e.getName()));
        super.start();
    }

    private void process() {
        for (@NotNull final File file : fileFolder.listFiles()) {
            if (file.isDirectory()) continue;
            @NotNull final String fileName = file.getName();
            final boolean check = commands.contains(fileName);
            if (check) {
                try {
                    file.delete();
                    bootstrap.processCommand(fileName);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

}
